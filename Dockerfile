FROM node:16-alpine
WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
RUN yarn install --production=true
EXPOSE 3000
RUN yarn build
CMD [ "yarn", "start" ]