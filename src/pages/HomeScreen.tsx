import React, { useCallback, useEffect, useState } from 'react';
import BookingCard from '../components/BookingCard';
import BookingTable from '../components/BookingTable';
import LoginCard from '../components/LoginCard';
import { User } from 'firebase/auth';
import useBooking from '../hooks/useBooking';
import { TailSpin } from 'react-loader-spinner';
import CovidNotificationModal from '../components/CovidNotificationModal';
import useArray from '../hooks/useArray';
import BookingInfo from '../components/BookingInfo';

type HomeScreenType = {
   user?: User;
   isLoggedUser: boolean;
   login: () => void;
   mustShowCovidNotificationModal: boolean;
   showCovidModal: (state: boolean) => void;
};

const HomeScreen = ({
   user,
   isLoggedUser,
   login,
   mustShowCovidNotificationModal,
   showCovidModal,
}: HomeScreenType) => {
   const {
      getBookings,
      deleteBooking,
      createBooking,
      getPastBookings,
      sendNotification,
      hasErrorCreateBooking,
      hasErrorSendNotification,
      bookings,
      loadingDeleteBooking,
      loadingGetBookings,
      loadingCreateBooking,
      loadingGetPastBookings,
      loadingSendNotification,
      successfulBooking,
      successfulNotification,
      createBookingErrorMessage,
      pastBookings,
      setSuccessfulNotification,
      setHasErrorSendNotification,
   } = useBooking();

   const handleDeletePress = useCallback(
      async (id) => {
         await deleteBooking(id);
         await getBookings();
      },
      [deleteBooking, getBookings],
   );

   const handleCloseModalPress = useCallback(() => {
      showCovidModal(false);
      setSuccessfulNotification(false);
      setHasErrorSendNotification(false);
   }, [showCovidModal, setSuccessfulNotification, setHasErrorSendNotification]);

   const [descriptionNotification, setDescriptionNotification] = useState('');
   const { array: datesNotification, push, remove, clear } = useArray([])
   const handleNotificationPress = useCallback(async () => {
      datesNotification.forEach(async (date) => {
         await sendNotification(user?.displayName, date, descriptionNotification)
      })
   }, [descriptionNotification, datesNotification, sendNotification, user?.displayName]);

   useEffect(() => {
      getBookings();
      // eslint-disable-next-line
   }, []);

   return (
      <>
         {isLoggedUser && !loadingGetBookings && (
            <>
               {mustShowCovidNotificationModal && (
                  <CovidNotificationModal
                     onClosePress={handleCloseModalPress}
                     pastBookings={pastBookings}
                     loadingGetPastBookings={loadingGetPastBookings}
                     getPastBookings={getPastBookings}
                     userName={user?.displayName}
                     onNotificationPress={handleNotificationPress}
                     descriptionNotification={descriptionNotification}
                     setDescriptionNotification={setDescriptionNotification}
                     setDatesNotification={push}
                     datesNotification={datesNotification}
                     unSetDatesNotification={remove}
                     clearDatesNotification={clear}
                     loadingSendNotification={loadingSendNotification}
                     hasErrorSendNotification={hasErrorSendNotification}
                     successfulNotification={successfulNotification}
                  />
               )}
               <BookingInfo />
               <BookingCard
                  getBookings={getBookings}
                  createBooking={createBooking}
                  loadingCreateBooking={loadingCreateBooking}
                  successfulBooking={successfulBooking}
                  hasErrorCreateBooking={hasErrorCreateBooking}
                  createBookingErrorMessage={createBookingErrorMessage}
                  user={user}
                  selectedDate={new Date()}
               />
               <BookingTable
                  bookings={bookings}
                  loadingDeleteBooking={loadingDeleteBooking}
                  userDisplayName={user?.displayName}
                  deleteBooking={handleDeletePress}
               />
            </>
         )}
         {loadingGetBookings && (
            <TailSpin
               arialLabel="loading-indicator"
               color="#fff"
               visible={true}
               height={30}
               width={30}
            />
         )}
         {!isLoggedUser && !loadingGetBookings && <LoginCard login={login} />}
      </>
   );
};

export default HomeScreen;
