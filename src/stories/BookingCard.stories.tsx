import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';
import { action } from '@storybook/addon-actions'

import BookingCard from './../components/BookingCard';

export default {
   title: 'BookingCard',
   component: BookingCard,
} as ComponentMeta<typeof BookingCard>;

export const Inicial: ComponentStory<typeof BookingCard> = () => <BookingCard
   getBookings={action('get bookings')}
   createBooking={action('create bookings')}
   loadingCreateBooking={false}
   successfulBooking={false}
   hasErrorCreateBooking={false}
   createBookingErrorMessage={''}
   user={undefined}
   selectedDate={new Date(2022, 1, 2)} />;


export const Exitoso: ComponentStory<typeof BookingCard> = () => <BookingCard
   getBookings={action('get bookings')}
   createBooking={action('create bookings')}
   loadingCreateBooking={false}
   successfulBooking={true}
   hasErrorCreateBooking={false}
   createBookingErrorMessage={''}
   user={undefined}
   selectedDate={new Date(2022, 1, 2)} />;

export const Erroneo: ComponentStory<typeof BookingCard> = () => <BookingCard
   getBookings={action('get bookings')}
   createBooking={action('create bookings')}
   loadingCreateBooking={false}
   successfulBooking={false}
   hasErrorCreateBooking={true}
   createBookingErrorMessage={'No se pudo realizar la reserva'}
   user={undefined}
   selectedDate={new Date(2022, 1, 2)} />;