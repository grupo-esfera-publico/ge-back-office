import React from 'react';
import Button from '../components/Button';

export default {
   title: 'Components/Button',
   component: Button,
};

const Template = (args: React.ComponentProps<typeof Button>) => (
   <Button {...args} />
);

export const Default = Template.bind({});
