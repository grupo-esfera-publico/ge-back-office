import React from 'react';
import './booking-info.css';

const BookingInfo = () => {
    return (
        <div className={'info bold-text'}>
            <h2>Aviso</h2>
            <ul>
                <li>A partir de marzo, los Lunes y Miércoles serán los días fijos que Celva y Alejo irán a la oficina, siempre y cuando haya 1 persona que se haya anotado hasta las 18hs del día hábil anterior. (Ej: para el lunes 14/02, hasta el Viernes 11/02 a las 18hs.)</li>
                <li >En tanto que los Martes y Jueves también irán si hay un mínimo de 5 personas siempre y cuando se hayan anotado hasta las 18hs del día previo al día hábil anterior. (Ej: para el Martes 15/02, hasta el Viernes 11/02 a las 18hs.)</li>
                <li >Celva y Alejo llegan a las 10hs</li>
                <li >Cualquier cosa llamá a Alejo en el horario hábil</li>
            </ul>

        </div>
    );
};

export default BookingInfo;