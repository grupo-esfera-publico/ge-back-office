import React, { useEffect, useCallback } from 'react';
import Button from './Button';
import './covid-notification-modal.css';
import { TailSpin } from 'react-loader-spinner';
import { DateObject } from '../config/types';

type CovidNotificationModalProps = {
   onClosePress: () => void;
   pastBookings: any;
   loadingGetPastBookings: boolean;
   getPastBookings: (username: string | undefined | null) => void;
   userName: string | undefined | null;
   onNotificationPress: () => void;
   descriptionNotification: string;
   setDescriptionNotification: (description: string) => void;
   setDatesNotification: (element: string) => void;
   datesNotification: string[];
   unSetDatesNotification: (element: string) => void;
   clearDatesNotification: () => void;
   loadingSendNotification: boolean;
   hasErrorSendNotification: boolean;
   successfulNotification: boolean;
};

const CovidNotificationModal = ({
   onClosePress,
   pastBookings,
   loadingGetPastBookings,
   getPastBookings,
   userName,
   onNotificationPress,
   descriptionNotification,
   setDescriptionNotification,
   setDatesNotification,
   datesNotification,
   unSetDatesNotification,
   clearDatesNotification,
   loadingSendNotification,
   hasErrorSendNotification,
   successfulNotification,
}: CovidNotificationModalProps) => {

   const handleCheckPastDate = useCallback((event) => {
      if (datesNotification.includes(event.target.value)) {
         unSetDatesNotification(event.target.value)
      } else {
         setDatesNotification(event.target.value)
      }

   }, [datesNotification, setDatesNotification, unSetDatesNotification]);

   useEffect(() => {
      getPastBookings(userName);
      clearDatesNotification();
      // eslint-disable-next-line
   }, []);

   const handleDescriptionNotificationChange = useCallback((event) => {
      setDescriptionNotification(event.target.value);
   }, [setDescriptionNotification])

   const getAssistantsNames = (assistants: any[]) => {
      const assistantsWithoutSender = assistants
         .filter((person: any) =>
            userName !== person.userDisplayName
         )
         .map((person: any) =>
            person.userDisplayName
         )

      const assistantsNames = assistantsWithoutSender.join(', ');
      return assistantsWithoutSender.length
         ? '(Avisarás a ' + assistantsNames + ')'
         : '(Fuiste la única persona en la oficina ese día)';
   }

   const formatDate = (date: DateObject): string => {
      return new Date(date.year, date.month, date.day).toISOString();
   }

   const isSendButtonDisabled = loadingSendNotification || successfulNotification || datesNotification.length === 0 || descriptionNotification.length === 0;

   return (
      <div className={'modal-container'}>
         <div className={'card modal'}>
            <h2 className={'card-title'}>Reportá tu posible contagio</h2>
            <p className={'covid-card-text'}>
               Deberías completar este form en caso de haber sido{' '}
               <span className="bold-text">
                  diagnosticado con COVID (hisopado positivo)
               </span>{' '}
               o en caso de ser{' '}
               <span className="bold-text">contacto estrecho*</span>.
            </p>
            <p className={'covid-card-text covid-card-clarification-text'}>
               *Tuviste contacto con un caso positivo por al menos 15 minutos a
               menos de 2mts mientras esa persona presentaba síntomas o dentro
               de las 48 horas anteriores a que le dieran el diagnóstico o
               comenzaran sus síntomas.
            </p>
            {loadingGetPastBookings ? (
               <TailSpin
                  arialLabel="loading-indicator"
                  color="#000"
                  visible={true}
                  height={16}
                  width={16}
               />
            ) : (
                  pastBookings.length ? (
                     <>
                        <ul className="content-notification-info">
                           {pastBookings.map((i: any) => (
                              <li key={i}>
                                 <input
                                    type="checkbox"
                                    onChange={handleCheckPastDate}
                                    disabled={i.assistants.length <= 1 || successfulNotification}
                                    value={formatDate(i.dateObject)}
                                 />
                                 <label>
                                    {i.date +
                                       ' ' + getAssistantsNames(i.assistants)
                                    }
                                 </label>
                              </li>
                           ))}
                        </ul>
                        <label className="bold-text">Breve descripción para las personas a las que se va a notificar</label>
                        <textarea className={'covid-text-area'} rows={6} maxLength={250} disabled={successfulNotification} onChange={handleDescriptionNotificationChange} />
                     </>
                  ) : (<p className={'content-notification-info error bold-text'}>
                     No asisitiste a la oficina en los últimos 10 días por lo tanto no hay necesidad de informar un posible contagio. Gracias!
                  </p>
                     )

               )}
            <div className="button-container">
               <div className="first-button">
                  <Button onClick={onNotificationPress} isDisabled={isSendButtonDisabled}>ENVIAR</Button>
               </div>
               {successfulNotification && (
                  <p className={'notification-message'}>Notificación exitosa!</p>
               )}
               {hasErrorSendNotification && (
                  <p className={'notification-message error'}>No se pudo enviar!</p>
               )}
               <div className="second-button">
                  <Button onClick={onClosePress} isDisabled={loadingSendNotification}>CERRAR</Button>
               </div>
            </div>
         </div>
      </div>
   );
};

export default CovidNotificationModal;
