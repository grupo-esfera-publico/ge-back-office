import React, { useCallback } from 'react';
import './header.css';
import Logo from '../img/logo-esfera-blanco.png';
import { User } from 'firebase/auth';
import Button from './Button';
import { featureCovidNotificationModalHidden } from '../config/toggleFeature';

type HeaderType = {
   user?: User;
   isLoggedUser: boolean;
   showCovidModal: (state: boolean) => void;
};

const Header = ({ user, isLoggedUser, showCovidModal }: HeaderType) => {
   const firstName = isLoggedUser
      ? user?.displayName?.split(' ')[0] + '!'
      : 'perri';

   const photoURL = user?.photoURL ? user.photoURL : undefined;

   const handleCovidNotificationPress = useCallback(() => {
      showCovidModal(true);
   }, [showCovidModal]);

   return (
      <nav>
         <img className="logo" src={Logo} alt="logo grupo esfera" />
         {isLoggedUser && (
            <div className="header-user-content">
               <div className="header-button-container" hidden={featureCovidNotificationModalHidden}>
                  <Button onClick={handleCovidNotificationPress}>
                     DAR AVISO DE POSIBLE CONTAGIO
                  </Button>
               </div>
               <div id="user-name-container">
                  <span className="greeting-text">Holi, {firstName}</span>
                  <img id="profile-avatar" src={photoURL} alt="profile" />
               </div>
            </div>
         )}
      </nav>
   );
};

export default Header;
