import { useState } from 'react';

const useArray = (defaultValue: any[]) => {
    const [array, setArray] = useState(defaultValue);

    const push = (element: any) => {
        setArray(originalArray => [...originalArray, element])
    }

    const clear = () => {
        setArray([])
    }

    const remove = (element: any) => {
        const index = array.find((value) => {
            return element === value
        })
        if (index && array.length > 1) {
            setArray(originalArray => [originalArray.slice(0, index), ...originalArray.slice(index + 1, originalArray.length - 1)])
        }
        else {
            clear();
        }
    }

    return { array, push, remove, clear }
}

export default useArray;