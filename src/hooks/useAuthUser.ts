import {
   User,
   signInWithPopup,
   GoogleAuthProvider,
   onAuthStateChanged,
} from 'firebase/auth';
import { authFirebase } from '../config/firebase-config';
import { useState } from 'react';

export const useAuthUser = () => {
   const [user, setUser] = useState<User>();
   const [isLoggedUser, setIsLoggedUser] = useState(false);

   onAuthStateChanged(authFirebase, (currentUser) => {
      const tempUser = currentUser ? currentUser : undefined;
      setUser(tempUser);
      setIsLoggedUser(currentUser !== null);
   });

   const login = () => {
      const provider = new GoogleAuthProvider();

      signInWithPopup(authFirebase, provider)
         .then((result) => {
            // const credential = GoogleAuthProvider.credentialFromResult(result);
            // const token = credential?.accessToken;
            setUser(result.user);
            setIsLoggedUser(true);
         })
         .catch((error) => {
            // const errorCode = error.code;
            // const errorMessage = error.message;
            // const email = error.email;
            // const credential = GoogleAuthProvider.credentialFromError(error);
            setIsLoggedUser(false);
         });
   };

   return { login, isLoggedUser, user };
};
