import React, { useState } from 'react';
import './App.css';
import Header from './components/Header';
import HomeScreen from './pages/HomeScreen';
import { useAuthUser } from './hooks/useAuthUser';

function App() {
   const { user, isLoggedUser, login } = useAuthUser();
   const [mustShowCovidNotificationModal, setMustShowCovidNotificationModal] =
      useState<boolean>(false);

   return (
      <div className={'app'}>
         <Header
            user={user}
            isLoggedUser={isLoggedUser}
            showCovidModal={setMustShowCovidNotificationModal}
         />
         <HomeScreen
            user={user}
            isLoggedUser={isLoggedUser}
            login={login}
            mustShowCovidNotificationModal={mustShowCovidNotificationModal}
            showCovidModal={setMustShowCovidNotificationModal}
         />
      </div>
   );
}

export default App;
