type UserWithMenu = {
   icon: string;
   userDisplayName: string;
   id: string;
   keyArrivalTime?: string;
};

export type DateObject = {
   day: number;
   month: number;
   year: number;
};

export type BookingsPerDate = {
   weekDay: string;
   date: string;
   dateObject: DateObject;
   assistants: UserWithMenu[];
};
