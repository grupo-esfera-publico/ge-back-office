# Docker
## Comando para construir
`docker build . -t grupo-esfera/back-office-fe`

## Comando para correr
`docker run --name ge-fe -p 3000:3000 grupo-esfera/back-office-fe`

## Pasos para subir la imagen al Registry de gitlab (se pueden copiar de https://gitlab.com/grupo-esfera-publico/ge-back-office/container_registry)
`docker login registry.gitlab.com (con el username y password propio de gitlab)`
`docker build -t registry.gitlab.com/grupo-esfera/productos/ge-back-office .`
`docker push registry.gitlab.com/grupo-esfera/productos/ge-back-office`

## Comandos para subir y correr en un server de Esfera
`ssh forky@192.168.88.103 (-> con clave de forky)`

`su -l (-> con clave de root)`

`docker login registry.gitlab.com (Estando logueado en el gitlab)`

`docker run --name app-reservas-oficina-fe -d -p 3001:3000 registry.gitlab.com/grupo-esfera-publico/ge-back-office:latest`

## Comando para actualizar la imagen a la última versión subida a la registry
`docker pull registry.gitlab.com/grupo-esfera-publico/ge-back-office:latest`

## Agregamos al haproxy.cfg las siguientes lineas
`acl es-oficina hdr_dom(host) -i oficina.grupoesfera.com.ar`
`use_backend oficina if es-oficina`
`backend oficina`
`  server oficina-server 192.168.88.103:3001 check`

## Luego reiniciamos el servicio
`sudo service haproxy restart`
